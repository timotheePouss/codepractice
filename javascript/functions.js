// ///////////////// TEST ALGO MIRUM - JS ///////////////// //

/* 1) Donnez un algo (autre que le cast python de base) permettant
de convertir une chaîne de caractère en entier.

ex en sortie d'une console python:

>>> stringToInt("-142")

-142
*/

function stringToInt(str) {
    let number = 0;
    let strArr = [...str];                      // create an array from string
    const positive = (isNaN(strArr[0] * 1) && strArr.shift() === '-' ? false : true);    // test if there is sign minus or positive & if so delete the NaN generating character
    let multiplier = 1;                         // initiate a multiplier to increase the integer
    for (const index in strArr.reverse()) {
        if (strArr.hasOwnProperty(index)) {     // check if not inherited properties
            const intValue = strArr[index] * 1;
            if (!isNaN(intValue)) {             // check if the string isn't a number
                number += (intValue * multiplier);
                multiplier *= 10;               // increment by 10, in order to place the numbers one before another
            } else {
                return `Invalid character '${strArr[index]}' detected inside the string provided, ${(strArr[index] === ',' || strArr[index] === '.' ? 'the number might be a float':'it could not be converted to number')}`
            }
        }
    }
    return (positive? number:number*-1);
}

/*
2) Supposons que nous ayons un tableau d'entiers ([4,8,8,5,1,4,5] par exemple),
    ou tous les nombres se répète deux fois, sauf un seul.
Donnez une fonction qui prend en entrée un tableau et qui renvoi le nombre en question ou une erreur si absent.
*/

function detectSingleItem(arr) {
    let result = Error(`Not any single occurence of a number in ${arr}`); // create error to return if no single value result found
    let doubled = [];
    for (const item of arr) {
        if (!doubled.includes(item)) {                      // check if not already validated double (for performance purpose)
            if (arr.filter(n => n === item).length === 2) { // check if doubled
                doubled.push(item);
            } else if (typeof result !== 'number'){         // if not already attributed, thus not single, then attributing the value,
                result = item;
            } else {                                        // else it will return an error & interrupt the function execution
                return Error(`More than one single occurence of a number in ${arr} => first found = ${result} & second found = ${item}`);
            }
        }
    }
    return result;
}


/*
3) (BONUS) – En vous servant uniquement de la fonction oneOrTwo() ci-dessous
    (et donc aucune autre méthode existante pour faire de l’aléatoire),
    faire la fonction dice() qui va renvoyer un chiffre entre 1 et 6 ( un dé à jouer donc)
    avec exactement les mêmes probabilités de tirage pour chaque chiffre. 
def oneOrTwo() :
        return random.randint(1, 2)

 */
const oneOrTwo = () => Math.floor(Math.random() * 2) + 1;

const dice = () => Math.floor(oneOrTwo() * (oneOrTwo() + oneOrTwo()/2));

function dice2 () {
    let result = 1;
    for(let i = 0; i <= 4; i++) {
        result = result + oneOrTwo()-1;
    }
    return result;
}

exports.stringToInt = stringToInt;
exports.detectSingleItem = detectSingleItem;
exports.dice = dice;
exports.dice2 = dice2;
