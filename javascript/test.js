const functions = require('./functions');

console.log('--------- Test for stringToInt ---------')

function testStringToInt(givenInput, expectedOutput) {
    const result = functions.stringToInt(givenInput);
    return `${(result === expectedOutput?'Success !':'Fail …')} Result = ${result} | Expected = ${expectedOutput}`
}
console.log(testStringToInt('-142', -142))
console.log(testStringToInt('142', 142))
console.log(testStringToInt('-14.2', "Invalid character '.' detected inside the string provided, the number might be a float"))

console.log('\n--------- Test for detectSingleItem ---------')

function testDetectSingleItem(givenInput, expectedOutput) {
    const result = functions.detectSingleItem(givenInput);
    return `Result = ${result}\n|-> Expected = ${expectedOutput}`
}
console.log(testDetectSingleItem([4,8,8,1,5,4,5], 1));
console.log(testDetectSingleItem([4,8,8,1,2,5,4,5], "Error: More than one single occurence of a number in …"));
console.log(testDetectSingleItem([4,8,8,5,4,5], "Error: Not any single occurence of a number in …"));

console.log('\n--------- Test for dice throw ---------')

function testDiceThrow(occurence){
    let diceNumbers =[0,0,0,0,0,0]
    for (let i = 0; i < occurence; i++) {
        diceNumbers[functions.dice()-1]++;
    }
    return `Dice throw n°1 = ${diceNumbers}`
}
function testDiceThrow2(occurence){   
    let diceNumbers =[0,0,0,0,0,0]
    for (let i = 0; i < occurence; i++) {
        diceNumbers[functions.dice2()-1]++;
    }
    return `Dice throw n°2 = ${diceNumbers}`
}
    
console.log(testDiceThrow(100))
console.log(testDiceThrow(100000))
console.log(testDiceThrow2(100))
console.log(testDiceThrow2(100000))
