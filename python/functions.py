# ################# TEST ALGO MIRUM - PY ################# #

# -*- coding: utf -*-

import math, random

""" 1) Donnez un algo (autre que le cast python de base) permettant
de convertir une chaîne de caractère en entier.

ex en sortie d'une console python:

>>> stringToInt("-142")

-142
"""


def isInt(char):
    print(char)

def stringToInt(strNumb):
    number = 0
    numbers = ['0','1','2','3','4','5','6','7','8','9']
    positive = False if (len(strNumb.split('-')) >= 2 ) else True
    strNumb = strNumb.split('-')[-1]
    multiplicator = 1
    for strN in reversed(strNumb):
        if (strN in numbers):
            number += numbers.index(strN) * multiplicator
            multiplicator *= 10
        elif (strN == "." or strN == ","):
            return "This number might correspond to a float, please give a string corresponding to an int"
    return number if positive else number * -1

def stringToIntTry(strNumb):
    number = 0
    numbers = ['0','1','2','3','4','5','6','7','8','9']
    positive = False if (len(strNumb.split('-')) >= 2 ) else True
    strNumb = strNumb.split('-')[-1]
    multiplicator = 1
    for strN in reversed(strNumb):
        try:
            number += numbers.index(strN) * multiplicator
            multiplicator *= 10
        except:
            if (strN == "." or strN == ","):
                return "This number might correspond to a float, please give a string corresponding to an int"
    return number if positive else number * -1


"""
2) Supposons que nous ayons un tableau d'entiers ([4,8,8,5,1,4,5] par exemple),
    ou tous les nombres se répète deux fois, sauf un seul.
Donnez une fonction qui prend en entrée un tableau et qui renvoi le nombre en question ou une erreur si absent.
"""
def detectSingleItem(arr):
    result = ValueError("Not any single occurence of a number in ${}".format(arr)) # create error to return if no single value result found
    doubled = []
    for item in arr:
        if (not item in doubled):
            if (len([n for n in arr if n == item]) == 2):
                doubled.append(item)
            elif (not isinstance(result, int)):
                result = item
            else:
                return ValueError("More than one single occurence of a number")
    return result

"""
3) (BONUS) – En vous servant uniquement de la fonction oneOrTwo() ci-dessous
    (et donc aucune autre méthode existante pour faire de l’aléatoire),
    faire la fonction dice() qui va renvoyer un chiffre entre 1 et 6 ( un dé à jouer donc)
    avec exactement les mêmes probabilités de tirage pour chaque chiffre. 
def oneOrTwo() :
        return random.randint(1, 2)
"""

def oneOrTwo():
    return random.randint(1, 2)

def dice():
    return math.floor(oneOrTwo() * (oneOrTwo() + oneOrTwo()/2))

def dice2():
    result = 1;
    for i in range(5):
        result = result + oneOrTwo()-1
    return result