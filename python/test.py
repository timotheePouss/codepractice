from functions import stringToInt, stringToIntTry, detectSingleItem, dice, dice2

def testStringToInt(strInput, expectedOutput):
    result = stringToInt(strInput) 
    if (result == expectedOutput):
        print(":-) Success for the input: {}\nResult output:   {}\nExpected output: {}\n".format(strInput, result, expectedOutput))
    else :
        print(":-( Fail for the input: {}\nResult output:   {}\nExpected output: {}\n".format(strInput, result, expectedOutput))

print('-------------- testStringToInt -----------------')
testStringToInt("-142", -142)
testStringToInt("- 142", -142)
testStringToInt(" -142", -142)
testStringToInt("142", 142)
testStringToInt(" 142", 142)
testStringToInt(" 1 4 2 ", 142)
testStringToInt(" 1B4C2 ", 142)
testStringToInt("-1,42", "This number might correspond to a float, please give a string corresponding to an int")

def testStringToIntTry(strInput, expectedOutput):
    result = stringToIntTry(strInput) 
    if (result == expectedOutput):
        print(":-) Success for the input: {}\nResult output:   {}\nExpected output: {}\n".format(strInput, result, expectedOutput))
    else :
        print(":-( Fail for the input: {}\nResult output:   {}\nExpected output: {}\n".format(strInput, result, expectedOutput))

print('-------------- testStringToIntTry -----------------')
testStringToIntTry('-142', -142)
testStringToIntTry('- 142', -142)
testStringToIntTry(' -142', -142)
testStringToIntTry('142', 142)
testStringToIntTry(' 142', 142)
testStringToIntTry(' 1 4 2 ', 142)
testStringToIntTry(' 1B4C2 ', 142)
testStringToIntTry('-1,42', 'This number might correspond to a float, please give a string corresponding to an int')

def testDetectSingleItem(givenInput, expectedOutput):
    result = detectSingleItem(givenInput)
    print(str(type(result)), result)
    if (result == expectedOutput):
        print(":-) Success for the input: {}\nResult output:   {}\nExpected output: {}\n".format(givenInput, result, expectedOutput))
    elif (str(type(result)).split("'")[1] == expectedOutput):
        print(":-) Success for the input: {}\nResult output:   {}\nExpected output: {}\n".format(givenInput, str(type(result)).split("'")[1], expectedOutput))
    else:
        print(":-( Fail, for the input: {}\nResult output:   {}\nExpected output: {}\n".format(givenInput, result, expectedOutput))

print('-------------- testDetectSingleItem -----------------')
testDetectSingleItem([4,8,8,1,5,4,5], 1)
testDetectSingleItem([4,8,-2,-2,5,4,5], 8)
testDetectSingleItem([4,8,5,5,4,5], 'ValueError')
testDetectSingleItem([4,8,8,1,2,5,4,5], 'ValueError')
testDetectSingleItem([4,8,8,5,4,5], 'ValueError')

def testDiceThrow(occurence):
    diceNumbers =[0,0,0,0,0,0]
    for i in range(occurence):
        diceNumbers[dice()-1]+= 1
    return "Dice throw n°1 = {}".format(diceNumbers)

def testDiceThrow2(occurence):
    diceNumbers =[0,0,0,0,0,0]
    for i in range(occurence):
        diceNumbers[dice2()-1]+= 1
    return "Dice throw n°2 = {}".format(diceNumbers)

print('-------------- testDiceThrow 1 -----------------')
print(testDiceThrow(100))
print(testDiceThrow(100000))

print('\n-------------- testDiceThrow 2 -----------------')
print(testDiceThrow2(100))
print(testDiceThrow2(100000))